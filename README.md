# Adarga NLP Data Scientist Interview

## Text Cleaning Exercise

In this exercise you will apply some logic to clean a string of text and validate your code using Unit Tests (PyTest).

## Repo 
- This repo contains a package called `text_cleaning_component`.
- The `test` directory contains the Unit Test to validate the above package. 
- A `requirements.txt` file can be found at the root of the repo.

# Task 
Your task is to:

1) Understand the code (python module specifically).

2) "Fix" the broken code in the python module so the Unit Tests pass. 

3) Recommend improvements to make this module production ready.

## How to run ##

## Setup Virtual Env (use conda or pyenv etc) and install required packages
```
$ cd ${PROJECT_ROOT}
pip install -r requirements.txt
```

### Run tests ###
From the root directory run
```
$ cd ${PROJECT_ROOT}
$ PYTHONPATH=. pytest ./test/unit/test_text_clean.py
```