import logging
import re

logging.basicConfig(level=logging.INFO)


class TextCleaning:
    @staticmethod
    def remove_whitespace(text: str) -> str:
        """
        Function to remove 2 or more spaces from the string
        :return: Cleaned string with 2 or more spaces removed
        """
        logging.debug("Text BEFORE remove extra space cleanup")
        logging.debug(text)
        clean_text_whitepace = text.strip()
        clean_text_whitepace = re.compile("  +").sub(" ", clean_text_whitepace)
        logging.debug("Text AFTER remove whitespace cleanup")
        logging.debug(clean_text_whitepace)
        logging.info("Remove whitespace step completed")
        return clean_text_whitepace

    @staticmethod
    def remove_newline_spacing(text: str) -> str:
        """
        Function to trim newline spaces
        :return: Cleaned string with newline trimmed
        """
        logging.debug("Text BEFORE remove newline space cleanup")
        logging.debug(text)
        clean_text_remove_newline_spacing = re.compile(
            "( *(?:\\n)+)+").sub(
            r"\n", text
        )
        logging.debug("Text AFTER remove newline space cleanup")
        logging.debug(clean_text_remove_newline_spacing)
        logging.info("Remove newline spacing step completed")
        return clean_text_remove_newline_spacing

    @classmethod
    def complete_cleanup(cls, text: str) -> str:
        """
        Function to completely clean string (above methods)
        :return: Cleaned string
        """
        logging.debug("Text BEFORE complete cleanup")
        logging.debug(text)
        cleaned_text = cls.remove_newline_spacing(text)
        cleaned_text = cls.remove_whitespace(cleaned_text)
        logging.debug("Text AFTER complete cleanup")
        logging.debug(cleaned_text)
        logging.info("COMPLETE CLEANUP STEP COMPLETED")
        return cleaned_text
