import pytest
from text_cleaning_component.text_clean import TextCleaning


@pytest.fixture
def txt_clean_init() -> object:
    def _method():
        txt_clean = TextCleaning()
        return txt_clean

    return _method


@pytest.fixture
def messy_text_1() -> str:
    messy_text_1 = (
        "Text of report by website of Somali privately-owned  Radio Dalsan on 4 August.  \n  \n \n\n"
        "   Ugandan Airlines has \n formally  announced that it will launch commercials flights between Kampala and Mogadishu on 28 August/ \n\n "
        "By BBC MonitoringAfghanistan's Attorney General's    Office (AGO) onSunday issued  \n an arrest warrant for former chief of Afghanistan Football.Federation (AFF) Keramuddin Karim."
    )
    return messy_text_1


@pytest.fixture
def clean_text_1() -> str:
    clean_text_1 = (
        "Text of report by website of Somali privately-owned Radio Dalsan on 4 August.\n"
        "Ugandan Airlines has\nformally announced that it will launch commercials flights between Kampala and Mogadishu on 28 August/\n"
        "By BBC MonitoringAfghanistan's Attorney General's Office (AGO) onSunday issued\nan arrest warrant for former chief of Afghanistan Football.Federation (AFF) Keramuddin Karim."
    )
    return clean_text_1


def test_remove_whitespace(txt_clean_init) -> None:
    cleaner = txt_clean_init()
    ex1_text = "    Hey there.   How's it    going?    "
    clean_text_extra_spaces = cleaner.remove_whitespace(ex1_text)
    assert clean_text_extra_spaces == "Hey there. How's it going?"


def test_remove_newline_spacing(txt_clean_init) -> None:
    cleaner = txt_clean_init()
    ex1_text = "Text of report by website of Somali:   \n    privately-owned Radio Dalsan on 4 August.   \n\n\n\n    Ugandan Airlines has formally  announced.  \n  that it will launch commercials/     \n     flights between Kampala and Mogadishu on 28 August."
    clean_text_newline_ex1 = cleaner.remove_newline_spacing(ex1_text)
    assert (
        clean_text_newline_ex1
        == "Text of report by website of Somali:\nprivately-owned Radio Dalsan on 4 August.\nUgandan Airlines has formally  announced.\nthat it will launch commercials/\nflights between Kampala and Mogadishu on 28 August."
    )


def test_complete_cleanup(txt_clean_init, messy_text_1, clean_text_1) -> None:
    cleaner = txt_clean_init()
    ex1_text = """Welcome to Adarga text \n \n\n cleaning services. \n \n  \n\n  How may  we help:  \n you?    """
    clean_text_complete_cleanup_ex1 = cleaner.complete_cleanup(ex1_text)
    assert (
        clean_text_complete_cleanup_ex1
        == """Welcome to Adarga text\ncleaning services.\nHow may we help:\nyou?"""
    )

    clean_text_complete_cleanup_ex2 = cleaner.complete_cleanup(messy_text_1)
    assert clean_text_complete_cleanup_ex2 == clean_text_1
