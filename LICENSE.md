# ADARGA LIMITED CONFIDENTIAL AND SECRET
Copyright (c) 2017 Adarga Limited, All Rights Reserved.

NOTICE: All information contained herein is, and remains the property of
Adarga Limited. The intellectual and technical concepts contained herein are
proprietary to Adarga Limited and may be covered by U.K. and foreign patents
or patent applications, and are protected by trade secret or copyright law.
Dissemination of this information or reproduction of this material is
strictly forbidden unless prior written permission is obtained from Adarga
Limited.
Access to the source code contained herein is hereby forbidden to anyone
except current Adarga Limited employees, managers or contractors who have
executed Confidentiality and Non-disclosure agreements explicitly covering
such access.

The copyright notice above does not evidence any actual or intended
publication or disclosure of this source code, which includes information
that is confidential and/or proprietary, and is a trade secret, of
Adarga Limited. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC
PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT
THE EXPRESS WRITTEN CONSENT OF ADARGA LIMITED IS STRICTLY PROHIBITED, AND IN
VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR
POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR
IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
